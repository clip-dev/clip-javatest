package br.com.meyer;


import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paulo on 06/07/15.
 */
public class TimeSlotsRepository {

    private static LocalDate generate() {
        long offset = 1438830000000l;
        long end = 1439175600000l;
        long diff = end - offset + 1;
        long rand = offset + (long)(Math.random() * diff);
        return new LocalDate(rand);
    }

    public static List<TimeSlot> getTimeSlots() {
        List<TimeSlot> timeSlots = new ArrayList<TimeSlot>();

        timeSlots.add(new TimeSlot("José", generate()));
        timeSlots.add(new TimeSlot("Anderson", generate()));
        timeSlots.add(new TimeSlot("Farinha", generate()));
        timeSlots.add(new TimeSlot("Victor", generate()));
        timeSlots.add(new TimeSlot("Rodrigo", generate()));
        timeSlots.add(new TimeSlot("Rafaele", generate()));
        timeSlots.add(new TimeSlot("Paulo", generate()));
        timeSlots.add(new TimeSlot("Josenildo", generate()));
        timeSlots.add(new TimeSlot("Eudjon", generate()));
        timeSlots.add(new TimeSlot("Mônica", generate()));
        timeSlots.add(new TimeSlot("Pedro", generate()));
        timeSlots.add(new TimeSlot("Renata", generate()));
        timeSlots.add(new TimeSlot("Kely", generate()));

        return timeSlots;
    }

}

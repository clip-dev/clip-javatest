package br.com.meyer;


import org.joda.time.LocalDate;

/**
 * Created by paulo on 06/07/15.
 */
public class TimeSlot implements Comparable<TimeSlot> {
    private String name;
    private LocalDate date;

    public TimeSlot(String name, LocalDate date) {
        this.name = name;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TimeSlot)) return false;

        TimeSlot timeSlot = (TimeSlot) o;

        if (name != null ? !name.equals(timeSlot.name) : timeSlot.name != null) return false;
        return !(date != null ? !date.equals(timeSlot.date) : timeSlot.date != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    public int compareTo(TimeSlot o) {
        return date.compareTo(o.date);
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "name='" + name + '\'' +
                ", date=" + date +
                '}';
    }
}
